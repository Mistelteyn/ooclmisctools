module.exports.routes = {
  '/': { view: 'pages/homepage' },
  'GET /getIterations': { controller: 'PairingMatrixController', action: 'getIterations' },
  'GET /getMembers': { controller: 'PairingMatrixController', action: 'getMembers' },
  'GET /getAllPairingHistory': { controller: 'PairingMatrixController', action: 'getAllPairingHistory' },
  'GET /getMembersByIteration/:iterationId?': 'PairingMatrixController.getMembersByIteration',
  'GET /getPairMatrixByIteration/:iterationId?': 'PairingMatrixController.getPairMatrixByIteration',
  'POST /addPairStory': 'PairingMatrixController.addPairStory'
};
