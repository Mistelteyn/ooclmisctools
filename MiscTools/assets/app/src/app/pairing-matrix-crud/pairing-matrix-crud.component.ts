import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Iteration } from '../pairing-matrix/classes/iteration'
import { PairingMatrixService } from '../services/pairing-matrix-service.service';
import { Members } from '../pairing-matrix/classes/members';
import { PairHistory } from '../pairing-matrix/classes/pair-history';
import { PairHistoryVO } from '../pairing-matrix/classes/pair-history-display';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { merge, Observable, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-pairing-matrix-crud',
  templateUrl: './pairing-matrix-crud.component.html',
  styleUrls: ['./pairing-matrix-crud.component.css']
})
export class PairingMatrixCRUDComponent implements AfterViewInit {
  iterations: Iteration[];
  members: Members[];
  pairStoryData: PairHistory;
  pairingHistory: PairHistoryVO;
  displayedColumns: string[] = ['select', 'PAIRING_HISTORY_ID', 'STORY_NUM', 'DESC', 'START_DATE', 'END_DATE', 'OWNER_NAME', 'PARTNER_NAME'];
  dataSource: MatTableDataSource<PairHistoryVO>;
  data: Iteration[];
  selection = new SelectionModel<PairHistoryVO>(true, []);
  isLoadingResults = true;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  constructor(private pairingMatrixService: PairingMatrixService) {
    this.pairStoryData = new PairHistory;
    this.pairingHistory = new PairHistoryVO
    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource();
  }

  ngOnInit() {


  }

  ngAfterViewInit() {
    this.getAllIteration();
    this.getMembers();
    this.getAllPairingHistory();

  }

  getAllIteration() {
    this.pairingMatrixService.getIteration().subscribe(
      data => {
        this.iterations = data;
      },
    );
  }

  getAllPairingHistory() {
    this.pairingMatrixService.getAllPairingHistory().subscribe(
      data => {
        this.isLoadingResults = false;
        this.dataSource = new MatTableDataSource(data);
      },
    );
  }

  getMembers() {
    this.pairingMatrixService.getMembers().subscribe(
      memberData => {
        this.members = memberData;
      },
    );
  }

  addPairStory() {
    this.pairingMatrixService.addPairStory(this.pairStoryData).subscribe(
      memberData => {
      },
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PairHistoryVO): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.PAIRING_HISTORY_ID + 1}`;
  }

}

