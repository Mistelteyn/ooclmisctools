import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PairingMatrixCRUDComponent } from './pairing-matrix-crud.component';

describe('PairingMatrixCRUDComponent', () => {
  let component: PairingMatrixCRUDComponent;
  let fixture: ComponentFixture<PairingMatrixCRUDComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PairingMatrixCRUDComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PairingMatrixCRUDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
