import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateDotbatchSqlComponent } from './create-dotbatch-sql/create-dotbatch-sql.component';
import { PairingMatrixComponent } from './pairing-matrix/pairing-matrix-table.component';
import { PairingMatrixCRUDComponent } from './pairing-matrix-crud/pairing-matrix-crud.component';
const routes: Routes = [
  { path: 'portal/createbatch', component: CreateDotbatchSqlComponent },
  { path: 'portal/pairmatrix', component: PairingMatrixComponent },
  { path: 'portal/editpairmatrix', component: PairingMatrixCRUDComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
