import { Injectable } from '@angular/core';
import { HttpClient,HttpParams,HttpHeaders } from '@angular/common/http';
import { AppSettings } from '../app.settings';
import { Observable } from 'rxjs';
import { PairHistory } from '../pairing-matrix/classes/pair-history';
import { PairHistoryVO } from '../pairing-matrix/classes/pair-history-display';
import { Members } from '../pairing-matrix/classes/members';
import { Iteration } from '../pairing-matrix/classes/iteration';
@Injectable({
  providedIn: 'root'
})
export class PairingMatrixService {

  constructor(private httpClient: HttpClient) { }

  getIteration():Observable<Iteration[]> {
    return this.httpClient.get<Iteration[]>(AppSettings.BACKEND_API_URL + '/getIterations');
  }

  getMembers():Observable<Members[]> {
    return this.httpClient.get<Members[]>(AppSettings.BACKEND_API_URL + '/getMembers');
  }
  getMembersByIteration(iterationId):Observable<Members[]>{
    let params1 = new HttpParams().set('iterationId', iterationId);
    return this.httpClient.get<Members[]>(AppSettings.BACKEND_API_URL + '/getMembersByIteration',{params:params1} );
  }
  getPairMatrixByIteration(iterationId):Observable<PairHistory[]> {
    let params1 = new HttpParams().set('iterationId', iterationId);
    return this.httpClient.get<PairHistory[]>(AppSettings.BACKEND_API_URL + '/getPairMatrixByIteration',{params:params1});
  }

  addPairStory(pairStoryData):Observable<PairHistory[]>{
    let headerOptions = new HttpHeaders();
    headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    
    return this.httpClient.post<PairHistory[]>(AppSettings.BACKEND_API_URL + '/addPairStory',JSON.stringify(pairStoryData),{headers: headerOptions});
  }

  getAllPairingHistory():Observable<PairHistoryVO[]> {
    return this.httpClient.get<PairHistoryVO[]>(AppSettings.BACKEND_API_URL + '/getAllPairingHistory');
  }



}
