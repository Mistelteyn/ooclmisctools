import { TestBed } from '@angular/core/testing';

import { PairingMatrixService } from './pairing-matrix-service.service';

describe('PairingMatrixService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PairingMatrixService = TestBed.get(PairingMatrixService);
    expect(service).toBeTruthy();
  });
});
