import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { PairingMatrixService } from '../services/pairing-matrix-service.service';
import { PairHistory } from './classes/pair-history';
import { Members } from './classes/members';
import { Iteration } from './classes/iteration';


@Component({
  selector: 'app-pairing-matrix',
  templateUrl: './pairing-matrix-table.component.html',
  styleUrls: ['./pairing-matrix-table.component.css']
})



export class PairingMatrixComponent implements OnInit {
  iterations: Iteration[];
  members: Members[];
  pair_history: PairHistory[];
  cards = null;




  constructor(private breakpointObserver: BreakpointObserver,
    private pairingMatrixService: PairingMatrixService
  ) { }

  ngOnInit() {
    this.getAllIteration();
    
  }

  loadPairMatrixByIteration(iterationId){
    this.getMembersAndPairMatrix(iterationId);
  }

  getAllIteration() {
    this.pairingMatrixService.getIteration().subscribe(
      data => {
        this.iterations = data;
      },
    );
  }
  getMembersAndPairMatrix(iterationId) {
    this.pairingMatrixService.getMembersByIteration(iterationId).subscribe(
      memberData => {
        this.members = memberData
        this.pairingMatrixService.getPairMatrixByIteration(iterationId).subscribe(
          pair_history => {
            this.pair_history = pair_history;
            this.mapMembers();
          }
        )
       
      },
    );
  }

  mapMembers() {
    
    this.cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
      map(({ matches }) => {
        let matrix = [];
        for (let x = 1; x <= this.members.length + 1; x++) {
          for (let y = 1; y <= this.members.length + 1; y++) {
            let cellData = {}
            cellData["name"] = null;
            cellData["paircount"] = null;
            if (x != y) {
              if (this.members[x - 2] && this.members[y - 2]) {
                let count = 0;
                for(let z = 1;z < this.pair_history.length+1;z++){
                  
                  if((this.members[x - 2].MEMBER_ID == this.pair_history[z-1].MEMBER_ID &&
                    this.members[y - 2].MEMBER_ID == this.pair_history[z-1].PARTNER_ID) ||
                    (this.members[x - 2].MEMBER_ID == this.pair_history[z-1].PARTNER_ID &&
                      this.members[y - 2].MEMBER_ID == this.pair_history[z-1].MEMBER_ID)
                    )
                    count++;
                }
                cellData["paircount"] = count;
              }

            }
            if (x != 1 && y == 1) {
              if (this.members[x - 2]) {
                cellData["name"] = this.members[x - 2].DOMAINNAME;
              }

            }
            if (x == 1 && y != 1) {
              if (this.members[y - 2]) {
                cellData["name"] = this.members[y - 2].DOMAINNAME;
              }

            }
            cellData["rows"] = 1;
            cellData["cols"] = 1;
            cellData["positionX"] = x;
            cellData["positionY"] = y;
            matrix.push(cellData)
          }
        }
        return matrix;
      })
    );
  }
}
