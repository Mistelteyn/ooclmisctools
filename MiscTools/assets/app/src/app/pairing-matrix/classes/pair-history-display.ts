export class PairHistoryVO{
    PAIRING_HISTORY_ID:number;
    STORY_NUM:string;
    DESC:string;
    ITERATION_NUM:number;
    START_DATE:Date;
    END_DATE:Date;
    OWNER_NAME:number;
    PARTNER_NAME:number;
}