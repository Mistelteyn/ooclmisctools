export class PairHistory{
    PAIRING_HISTORY_ID:number;
    STORY_NUM:string;
    DESC:string;
    ITERATION_ID:number;
    MEMBER_ID:number;
    PARTNER_ID:number;
}