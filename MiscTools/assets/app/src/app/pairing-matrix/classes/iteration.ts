export class Iteration{
    ITERATION_ID:number;
    ITERATION_NUM:number;
    START_DATE:Date;
    END_DATE:Date;
}