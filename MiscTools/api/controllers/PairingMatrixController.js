const mysql = require('mysql2/promise');

// create the connection to database
const connection = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: 'mysqlpassword',
  port: '3306',
  database: 'misctoolsdb'
});



module.exports = {
  getIterations: async function (req, res) {
    // simple query
    let result = await connection.query(
      'SELECT * FROM iteration',
    );
    setResponseHeader(res);
    return res.json(result[0]);;
  },

  getMembers: async function (req, res) {
    let result = await connection.query(
      'SELECT A.MEMBER_ID, A.DOMAINNAME FROM MEMBERS A'
    );
    setResponseHeader(res);
    return res.json(result[0]);;
  },
  getMembersByIteration: async function (req, res) {
    let result = await connection.query(
      'SELECT A.MEMBER_ID, B.DOMAINNAME FROM PAIRING_HISTORY A INNER JOIN MEMBERS B ON ' +
      'A.MEMBER_ID = B.MEMBER_ID WHERE ITERATION_ID = ' + req.param("iterationId") + ' UNION ' +
      'SELECT B.MEMBER_ID,B.DOMAINNAME FROM PAIRING_HISTORY A INNER JOIN MEMBERS B ON ' +
      ' A.PARTNER_ID = B.MEMBER_ID WHERE ITERATION_ID = ' + req.param("iterationId")
    );
    setResponseHeader(res);
    return res.json(result[0]);;
  },
  getAllPairingHistory: async function (req, res) {
    console.log("fudge");
    let result = await connection.query(
      'SELECT A.PAIRING_HISTORY_ID,A.STORY_NUM,A.DESC,'+
      '(SELECT ITERATION_NUM FROM ITERATION WHERE ITERATION_ID = a.ITERATION_ID) AS ITERATION_NUM,'+
      '(SELECT START_DATE FROM ITERATION WHERE ITERATION_ID = a.ITERATION_ID) AS START_DATE,'+
      '(SELECT END_DATE FROM ITERATION WHERE ITERATION_ID = a.ITERATION_ID) AS END_DATE,'+
      '(SELECT DOMAINNAME FROM MEMBERS WHERE MEMBER_ID = A.MEMBER_ID) AS OWNER_NAME,' + 
      '(SELECT DOMAINNAME FROM MEMBERS WHERE MEMBER_ID = A.PARTNER_ID) AS PARTNER_NAME ' +
      'FROM PAIRING_HISTORY A'
    );
    setResponseHeader(res);
    return res.json(result[0]);;
  },
  getPairMatrixByIteration: async function (req, res) {
    let result = await connection.query(
      'SELECT * FROM PAIRING_HISTORY WHERE ITERATION_ID = ' + req.param("iterationId")
    );
    setResponseHeader(res);
    return res.json(result[0]);
  },
  addPairStory: async function (req, res) {
    let pairStory = req.body;
    let result = await connection.query(
      'INSERT INTO PAIRING_HISTORY (`STORY_NUM`,`DESC`,`MEMBER_ID`,`PARTNER_ID`,`ITERATION_ID`) VALUES(\'' + pairStory.STORY_NUM + '\',\'' + pairStory.DESC + '\',' + pairStory.MEMBER_ID + ',' + pairStory.PARTNER_ID + ',' + pairStory.ITERATION_ID + ')'
    );
    setResponseHeader(res);
    return res.json(result[0]);
  }

};

function setResponseHeader(res) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
  res.setHeader('Access-Control-Allow-Credentials', true); // If needed
  return res;
};

