-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5332
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for misctoolsdb
CREATE DATABASE IF NOT EXISTS `misctoolsdb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `misctoolsdb`;

-- Dumping structure for table misctoolsdb.iteration
CREATE TABLE IF NOT EXISTS `iteration` (
  `ITERATION_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ITERATION_NUM` int(11) DEFAULT NULL,
  `START_DATE` date NOT NULL,
  `END_DATE` date NOT NULL,
  PRIMARY KEY (`ITERATION_ID`),
  UNIQUE KEY `START_DATE` (`START_DATE`),
  UNIQUE KEY `END_DATE` (`END_DATE`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

-- Dumping data for table misctoolsdb.iteration: ~26 rows (approximately)
/*!40000 ALTER TABLE `iteration` DISABLE KEYS */;
REPLACE INTO `iteration` (`ITERATION_ID`, `ITERATION_NUM`, `START_DATE`, `END_DATE`) VALUES
	(14, 1, '2019-01-07', '2019-01-18'),
	(15, 2, '2019-01-21', '2019-02-01'),
	(16, 3, '2019-02-04', '2019-02-15'),
	(17, 4, '2019-02-18', '2019-03-01'),
	(18, 5, '2019-03-04', '2019-03-15'),
	(19, 6, '2019-03-18', '2019-03-29'),
	(20, 7, '2019-04-01', '2019-04-12'),
	(21, 8, '2019-04-15', '2019-04-26'),
	(22, 9, '2019-04-29', '2019-05-10'),
	(23, 10, '2019-05-13', '2019-05-24'),
	(24, 11, '2019-05-27', '2019-06-07'),
	(25, 12, '2019-06-10', '2019-06-21'),
	(26, 13, '2019-06-24', '2019-07-05'),
	(27, 14, '2019-07-08', '2019-07-19'),
	(28, 15, '2019-07-22', '2019-08-02'),
	(29, 16, '2019-08-05', '2019-08-16'),
	(30, 17, '2019-08-19', '2019-08-30'),
	(31, 18, '2019-09-02', '2019-09-13'),
	(32, 19, '2019-09-16', '2019-09-27'),
	(33, 20, '2019-09-30', '2019-10-11'),
	(34, 21, '2019-10-14', '2019-10-25'),
	(35, 22, '2019-10-28', '2019-11-08'),
	(36, 23, '2019-11-11', '2019-11-22'),
	(37, 24, '2019-11-25', '2019-12-06'),
	(38, 25, '2019-12-09', '2019-12-20'),
	(39, 26, '2019-12-23', '2020-01-03');
/*!40000 ALTER TABLE `iteration` ENABLE KEYS */;

-- Dumping structure for table misctoolsdb.members
CREATE TABLE IF NOT EXISTS `members` (
  `MEMBER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOMAINNAME` char(50) DEFAULT NULL,
  PRIMARY KEY (`MEMBER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table misctoolsdb.members: ~6 rows (approximately)
/*!40000 ALTER TABLE `members` DISABLE KEYS */;
REPLACE INTO `members` (`MEMBER_ID`, `DOMAINNAME`) VALUES
	(1, 'ESPALNO'),
	(2, 'DESENKE2'),
	(3, 'PUNZAGH'),
	(4, 'CASTICH'),
	(5, 'BRUGAJE'),
	(6, 'JOSECH2'),
	(7, 'NICOLCH');
/*!40000 ALTER TABLE `members` ENABLE KEYS */;

-- Dumping structure for table misctoolsdb.pairing_history
CREATE TABLE IF NOT EXISTS `pairing_history` (
  `PAIRING_HISTORY_ID` int(11) NOT NULL AUTO_INCREMENT,
  `STORY_NUM` varchar(50) NOT NULL DEFAULT '0',
  `DESC` varchar(3000) DEFAULT NULL,
  `ITERATION_ID` int(11) NOT NULL DEFAULT '0',
  `MEMBER_ID` int(11) NOT NULL,
  `PARTNER_ID` int(11) DEFAULT '0',
  PRIMARY KEY (`PAIRING_HISTORY_ID`),
  KEY `MEMBER_ID` (`MEMBER_ID`),
  KEY `PARTNER_ID` (`PARTNER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table misctoolsdb.pairing_history: ~6 rows (approximately)
/*!40000 ALTER TABLE `pairing_history` DISABLE KEYS */;
REPLACE INTO `pairing_history` (`PAIRING_HISTORY_ID`, `STORY_NUM`, `DESC`, `ITERATION_ID`, `MEMBER_ID`, `PARTNER_ID`) VALUES
	(1, 'ST111223', '0', 14, 1, 2),
	(2, 'ST111224', '0', 14, 3, 4),
	(3, 'ST111225', '0', 14, 1, 3),
	(4, 'ST111226', '0', 14, 2, 3),
	(5, 'ST111228', '0', 14, 1, 2),
	(6, 'ST111227', '0', 14, 1, 3),
	(7, 'ST111224', '0', 14, 3, 1),
	(8, 'ST123456', 'DESCTEST', 14, 1, 2),
	(9, 'ST123456', 'DESCTEST', 15, 1, 2);
/*!40000 ALTER TABLE `pairing_history` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
